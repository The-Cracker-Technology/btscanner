#!/bin/sh

if [ -f Makefile ]; then
	echo "make distclean"
	make distclean
fi

rm -rf aclocal.m4 autom4te.cache config.h configure depcomp
rm -rf install-sh Makefile.in missing mkinstalldirs

echo "aclocal"
aclocal
if [ $? != 0 ]; then
	echo "aclocal failed"
	exit 1
fi

echo "autoheader"
autoheader
if [ $? != 0 ]; then
	echo "autoheader failed"
	exit 1
fi

echo "automake -a -c -f"
automake -a -c -f
if [ $? != 0 ]; then
	echo "automake failed"
	exit 1
fi

echo "autoconf"
autoconf
if [ $? != 0 ]; then
	echo "autoconf failed"
	exit 1
fi

if [ -x ./configure ]; then
	echo "Running configure"
	./configure
fi

