/*
 * btscanner - Displays the output of Bluetooth scans
 * Copyright (C) 2003 Pentest Limited
 * 
 * Written 2003 by Tim Hurman <timh at pentest.co.uk>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY
 * RIGHTS.  IN NO EVENT SHALL THE COPYRIGHT HOLDER(S) AND AUTHOR(S) BE LIABLE
 * FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY
 * DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * 
 * ALL LIABILITY, INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PATENTS,
 * COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS, RELATING TO USE OF THIS SOFTWARE
 * IS DISCLAIMED.
 */

/*
 * misc.c: misc functions; signal handlers and alike
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <pthread.h>

#include <misc.h>
#include <log.h>

/* convert a bdaddr to an uint64_t */
uint64_t bd2int(bdaddr_t *bd)
{
	uint64_t ret=0;

	ret = bd->b[5];
	ret <<= 8;
	ret |= bd->b[4];
	ret <<= 8;
	ret |= bd->b[3];
	ret <<= 8;
	ret |= bd->b[2];
	ret <<= 8;
	ret |= bd->b[1];
	ret <<= 8;
	ret |= bd->b[0];
	return ret;
}

/* and the reverse */
int int2bd(uint64_t i, bdaddr_t *bd)
{
	bd->b[0] = i & 0xff;
	i >>= 8;
	bd->b[1] = i & 0xff;
	i >>= 8;
	bd->b[2] = i & 0xff;
	i >>= 8;
	bd->b[3] = i & 0xff;
	i >>= 8;
	bd->b[4] = i & 0xff;
	i >>= 8;
	bd->b[5] = i & 0xff;
	return 0;
}

