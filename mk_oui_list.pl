#!/usr/bin/perl

use strict;
use constant OUI_TXT_IN => "oui.txt.in";
use constant OUI_TXT_OUT => "oui.txt";
no locale;
no utf8;

if (! -r OUI_TXT_IN) {
	print "Input file ".OUI_TXT_IN." does not exist\n";
	exit(1);
}

my ($in, $out);
unless(open($in, OUI_TXT_IN)) {
	print "Unable to open input file ".OUI_TXT_IN.": $!\n";
	exit(1);
}
unless(open($out, ">".OUI_TXT_OUT)) {
	print "Unable to open output file ".OUI_TXT_OUT.": $!\n";
	exit(1);
}

my ($tmp);
while($tmp = <$in>) {
	$tmp =~ s/\s+$//;
	if ($tmp =~ m/^([0-9a-zA-Z]{6})\s+\(base 16\)\s+(.+)$/o) {
		print $out "$1 $2\n";
	}
}

close ($in);
close ($out);
exit(0);
