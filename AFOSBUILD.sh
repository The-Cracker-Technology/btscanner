find ./ -type f -exec sed -i -e 's/-Wimplicit-function-dec//g' {} \;

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Disable implicit function declaration warning... PASS!"
else
  # houston we have a problem
  exit 1
fi

./autogen.sh

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Autogen.sh PASS!"
else
  # houston we have a problem
  exit 1
fi

./configure

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Configure... PASS!"
else
  # houston we have a problem
  exit 1
fi

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

make install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make install... PASS!"
else
  # houston we have a problem
  exit 1
fi
