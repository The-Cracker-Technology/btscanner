# Process this file with autoconf to produce a configure script.

AC_INIT(main.c)
AM_INIT_AUTOMAKE(btscanner, 2.1)
AC_CONFIG_HEADERS([config.h])

# Checks for programs.
AC_PROG_CC
AC_PATH_PROGS(PERL, perl perl5.004 perl5.003 perl5.002 perl5.001 perl5)
AC_ISC_POSIX

# Checks for libraries.
AC_HAVE_LIBRARY(ncurses,,[AC_MSG_ERROR([ncurses not found])])
AC_HAVE_LIBRARY(menu,,[AC_MSG_ERROR([ncurses menu library not found])])
AC_HAVE_LIBRARY(form,,[AC_MSG_ERROR([ncurses form library not found])])
AC_HAVE_LIBRARY(bluetooth,,[AC_MSG_ERROR([Bluetooth not found])])
AC_HAVE_LIBRARY(pthread,,[AC_MSG_ERROR([pthreads not found])])

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([netinet/in.h stdlib.h string.h sys/ioctl.h \
  sys/socket.h unistd.h fcntl.h stdarg.h syslog.h])
AC_CHECK_HEADERS([bluetooth/bluetooth.h],,
  [AC_MSG_ERROR([Bluetooth headers MIA])])
AC_CHECK_HEADERS([bluetooth/sdp.h],,
  [AC_MSG_ERROR([SDP headers MIA])])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_SIZE_T

# Checks for library functions.
AC_PROG_GCC_TRADITIONAL
AC_FUNC_MALLOC
AC_TYPE_SIGNAL
AC_CHECK_FUNCS([memset strerror])

# set the optimisation/debug levels in CFLAGS
#CFLAGS="-g -ggdb"

# config for libxml2 check
AC_ARG_WITH(xml2cfgpath,
  [  --with-libxml2=dir      Base directory where libxml2 is installed],
  [ xml2cfgpath="${withval}/bin" ])
# check for libxml2
if test -z "$xml2cfgpath"
then
	AC_PATH_PROG(xml2cfg,xml2-config,"no")
else
	AC_PATH_PROG(xml2cfg,xml2-config,"no", $xml2cfgpath)
fi
if test "$xml2cfg" = "no"
then
	AC_MSG_ERROR([libxml2 not found or installed])
else
	CFLAGS="$CFLAGS `$xml2cfg --cflags`"
	CPPFLAGS="$CPPFLAGS `$xml2cfg --cflags`"
	LDFLAGS="$LDFLAGS `$xml2cfg --libs`"
fi
# check for libxml2 heads/libs
AC_CHECK_HEADERS([libxml/parser.h libxml/tree.h],,
  [AC_MSG_ERROR([libxml2 headers MIA])])
AC_HAVE_LIBRARY(xml2,,[AC_MSG_ERROR([libxml2 not found])])

# CFLAGS
CFLAGS="$CFLAGS -Wall -pthread -Wshadow -Wbad-function-cast \
  -Wformat -Wimplicit-function-dec -Wparentheses -Wsign-compare \
  -Wstrict-prototypes -Wtrigraphs -Wundef -Wuninitialized \
  -W -Wunused -Wformat-security -Wmissing-braces -Wbad-function-cast \
  -Wcast-qual -falign-functions -falign-labels -falign-loops\
  -pedantic -fstrict-aliasing -D_GNU_SOURCE -std=c99"
#  -fstack-check -fbounds-check"

# cfg
AC_MSG_CHECKING(the location of the config file)
AC_ARG_WITH(cfgfile,
  [  --with-cfg=file        location of the config file],
  [ cfgfile="${withval}" ],
  [ cfgfile="$sysconfdir/btscanner.xml" ])
AC_MSG_RESULT($cfgfile)
# cfg DTD
AC_MSG_CHECKING(the location of the config DTD)
AC_ARG_WITH(cfgdtd,
  [  --with-dtd=file        location of the config dtd],
  [ cfgdtd="${withval}" ],
  [ cfgdtd="file://$sysconfdir/btscanner.dtd" ])
AC_MSG_RESULT($cfgdtd)

CFLAGS="$CFLAGS -DCFG_FILE=\\\"$cfgfile\\\" -DCFG_DTD=\\\"$cfgdtd\\\""

# output
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
